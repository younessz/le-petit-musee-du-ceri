package fr.youness.myproject.data;

import android.util.Log;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Response;

public class ItemResult {


    static void transferInfo(ItemResponse response,Item item) {
        item.setName(response.name);
        item.setDescription(response.description);
        item.setCategories(""+response.categories);
        item.setBrand(response.brand);
        item.setYear(response.year);
        item.setTimeFrame(""+response.timeFrame);
        item.setTechnicalDetails(""+response.technicalDetails);
        item.setWorking(response.working);
    }
}
