package fr.youness.myproject.data.webservice;

import fr.youness.myproject.data.ItemResponse;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
public interface CERIInterface {

    @Headers("Accept: application/json")
    @GET("/cerimuseum/collection")
    Call<Map< String , ItemResponse>> getCollection();
}

