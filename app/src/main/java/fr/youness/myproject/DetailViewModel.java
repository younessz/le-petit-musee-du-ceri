package fr.youness.myproject;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import fr.youness.myproject.data.CeriRepository;
import fr.youness.myproject.data.Item;

public class DetailViewModel extends AndroidViewModel {
    private CeriRepository repository;
    private MutableLiveData<Item> item;

    public DetailViewModel(@NonNull Application application) {
        super(application);
        repository = CeriRepository.get(application);
        item = new MutableLiveData<>();
    }

    LiveData<Item> getItem() {
        return item;
    }


    public void setItem(long id) {

        repository.getItem(id);
        item = repository.getSelectedItem();
    }
}
